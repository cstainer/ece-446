# Assignment 
  - ECE 446 Device Driver Development
  - Homework 5
  - Charles Stainer
  - December 9, 2020

# Build
Download and unzip `hw5.zip`, or clone this Gitlab repo:

```sh
$ git clone git@gitlab.com:cstainer/ece-446.git
```

Navigate to the downloaded directory, then run `make` to compile the module. In this case, I cloned the repo to (or downloaded and unzipped on) my desktop.

```sh
$ cd Desktop/ece-446/hw5/user/
$ make
```

# Load
You do not need to load the module, the user program will do that for you.

# Unload
You do not need to unload the module, the user program will do that for you.

# Running the user program
To run the user program from `hw5/user` run:
```bash
$ sudo ./user
```
It is important that you run the program as `sudo`, as there are various functions and bash scripts called in the course of the program that require super user privilages.

# Testing
Once you've run the program, it will ask you to enter the runner information for at least 2 and up to 8 runners. Enter each runner's name, school name, and qualifying time in seconds. The runner's name and school name must be strings up to 20 characters in length.  The qualifying time must be a double or int not larger than 999.999. Included is my test data:
```
George Russell
Williams High
53.377

Daniel Ricciardo
Renault Prep
53.957

Kimi Raikkonen
Alfa Romeo Academy
54.963
```

After the second runner's information is entered, the program will ask you if you wish to enter further runners in the race.  Enter `y` or `n` for 'yes' or 'no'.  After entering an 8th runner, the program will not ask for or allow further runners to be entered.

At this point the program is ready to begin the race, and will prompt you for the start command.  When you are ready to being the race, enter `START` in all caps.  The program will output a table of entered runners and count down to the beginning of the race.

The program will output data after each lap of the race has been completed.  I have arbitrarily decided that the race will be 4 laps long.  After the last lap has been completed, the program will output a table of the finishers sorted by ascending total time, and then exit.

# Test output with provided data
```
Welcome to the ECE 446 HW5 Race Timing System!
Enter the information for at least 2 and up to 8 runners.
Enter the information the information for runner in lane 1.
Enter runner's name: George Russell
Enter runner's school: Williams High
Enter runner's qualifying time: 53.377

Enter the information the information for runner in lane 2.
Enter runner's name: Daniel Ricciardo
Enter runner's school: Renault Prep
Enter runner's qualifying time: 53.957

Would you like to add another runner (y/n)? y

Enter the information the information for runner in lane 3.
Enter runner's name: Kimi Raikkonen
Enter runner's school: Alfa Romeo Academy
Enter runner's qualifying time: 54.963

Would you like to add another runner (y/n)? n

To start the race, enter START
START

Runners, to your positions!
Name                | School              | Lane | Qual Time |
====================|=====================|======|===========|
George Russell      | Williams High       |   1  |   53.377  |
Daniel Ricciardo    | Renault Prep        |   2  |   53.957  |
Kimi Raikkonen      | Alfa Romeo Academy  |   3  |   54.963  |

3
2
1
GO!

Name                | Lane |  Lap 1  |  Total  |
====================|======|=========|=========|
George Russell      |   1  |  60.000 |  60.000 |
Daniel Ricciardo    |   2  |  59.996 |  59.996 |
Kimi Raikkonen      |   3  |  59.992 |  59.992 |

Name                | Lane |  Lap 1  |  Lap 2  |  Total  |
====================|======|=========|=========|=========|
George Russell      |   1  |  60.000 |  60.000 | 120.000 |
Daniel Ricciardo    |   2  |  59.996 |  59.996 | 119.992 |
Kimi Raikkonen      |   3  |  59.992 |  59.992 | 119.984 |

Name                | Lane |  Lap 1  |  Lap 2  |  Lap 3  |  Total  |
====================|======|=========|=========|=========|=========|
George Russell      |   1  |  60.000 |  60.000 |  60.000 | 180.000 |
Daniel Ricciardo    |   2  |  59.996 |  59.996 |  59.996 | 179.988 |
Kimi Raikkonen      |   3  |  59.992 |  59.992 |  59.992 | 179.976 |

Name                | Lane |  Lap 1  |  Lap 2  |  Lap 3  |  Lap 4  |  Total  |
====================|======|=========|=========|=========|=========|=========|
George Russell      |   1  |  60.000 |  60.000 |  60.000 |  60.000 | 240.000 |
Daniel Ricciardo    |   2  |  59.996 |  59.996 |  59.996 |  59.996 | 239.984 |
Kimi Raikkonen      |   3  |  59.992 |  59.992 |  59.992 |  59.992 | 239.968 |


Final Result:
Pos | Name                | School             | Lane |  Total  |
====|=====================|====================|======|=========|
 1  | Kimi Raikkonen      | Alfa Romeo Academy  |   3  |  239.968  |
 2  | Daniel Ricciardo    | Renault Prep        |   2  |  239.984  |
 3  | George Russell      | Williams High       |   1  |  240.000  |

```

# Locating the module's messages
For ease of sifting through the zillion lines of `/var/log/syslog`, I included `(hw5)` at the beginning of each printout.  In `/var/log/` run:

```sh
$ cat syslog | grep hw5
```
Note that you should only see a message indicating successful load and unload of the module. If you encounter errors in running the kernel module, it will have additional printouts indicating what failed.