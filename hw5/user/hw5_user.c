
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>

#define U2K _IOW('z', 'a', int32_t*)	/* copy_from_user, user to kernel */
#define K2U _IOR('z', 'b', int32_t*)	/* copy_to_user, kernel to user */

#define SEC(j) (j/250.0)				/* convernsion from int jiffies to double seconds */

#define LAPS 4							/* number of laps in the race */

/*
 * Represents one runner and contains all associated data in a doubly linked list (dll)
 */
struct node 
{
	int lane_number;
	char name[20];
	char school[20];
	double qual_time;
	double lap_time[LAPS];
	double total_time;

	struct node* next;
	struct node* prev;
};

/* 
 * Add a node to the end of the dll accessed by head
 */
void append(struct node** head,
			char* new_name,
			char* new_school,
			double new_qual_time)
{
	/* allocate memory for new node */
	struct node* new_node = (struct node*) malloc(sizeof(struct node));
	struct node *last = *head;

	/* populate data fields of new node */
	strcpy(new_node->name, new_name);
	strcpy(new_node->school, new_school);
	new_node->qual_time = new_qual_time;
	new_node->next = NULL;
	new_node->total_time = 0.0;

	for(int i = 0; i < 4; i++)
		new_node->lap_time[i] = 0.0;

	/* if the list is currently empty, make the new node the head of the list */
	if (*head == NULL)
	{
		new_node->lane_number = 1;
		new_node->prev = NULL;
		*head = new_node;
	}
	else
	{
		/* else iterate through the list until the last node */
		while (last->next != NULL)
		{
			last = last->next;
		}

		/* append the new node to the last node and populate the new node's lane number */
		last->next = new_node;
		new_node->lane_number = last->lane_number + 1;
		new_node->prev = last;
	}
}

/*
 * Update the latest lap time of a single runner, accessed by head and found by lane number
 */
void update_lap_time(struct node** head, int lane, double new_lap_time)
{
	struct node* temp = *head;

	/* iterate through the list until we find the correct lane nubmer */
	while ((temp != NULL) && (temp->lane_number != lane))
		temp = temp->next;

	/*  if we have not reached the end of the list without finding the lane number to update */
	if (temp != NULL)
	{
		/* iterate through the node's lap time array until we reach the current lap */
		int i = 0;
		while (temp->lap_time[i] != 0.0)
		{
			i++;

			/* do not exceed the bounds of the array */
			if (i > LAPS)
				return;	/* return without updating the last lap */
		}

		/* update lastest lap and total race time */
		temp->lap_time[i] = new_lap_time;
		temp->total_time += new_lap_time;
	}
}

/*
 * Prints a formatted representation of data fields that are filled at the beginning of a race
 */
void print_start(struct node** head)
{
	struct node* temp = *head;

	/* print header */
	printf("\nRunners, to your positions!\n");
	printf("Name                | School              | Lane | Qual Time |\n");
	printf("====================|=====================|======|===========|\n");

	/* for each runner */
	while (temp != NULL)
	{
		/* print name */
		printf("%s", temp->name);

		/* fill empty spaces */
		for (int i = 0; i < (20 - strlen(temp->name)); i++)
			printf(" ");

		/* print school */
		printf("| %s", temp->school);

		/* fill empty spaces */
		for (int j = 0; j < (20 - strlen(temp->school)); j++)
			printf(" ");

		/* print lane number */
		printf("|   %d  |", temp->lane_number);

		/* print qual time */
		if (temp->qual_time < 100.0)
			printf(" ");

		printf("  %0.3lf  |\n", temp->qual_time);

		/* increment to the next runner */
		temp = temp->next;
	}

	printf("\n");
}

/*
 * Prints a formatted representation of data fields that are filled during the race
 */
void print_current(struct node** head)
{
	int laps_complete = LAPS - 1;
	struct node* temp = *head;

	/* find the number of laps that have been completed */
	while (temp->lap_time[laps_complete] == 0.0)
		laps_complete--;

	laps_complete++;

	/* print header */
	printf("Name                | Lane |");

	for (int i = 0; i < laps_complete; i++)
		printf("  Lap %d  |", i+1);

	printf("  Total  |\n");
	printf("====================|======|");
	
	for (int j = 0; j < laps_complete; j++)
		printf("=========|");

	printf("=========|\n");

	/* for each runner */
	while (temp != NULL)
	{
		/* print name */
		printf("%s", temp->name);

		/* fill empty spaces */
		for (int k = 0; k < (20 - strlen(temp->name)); k++)
			printf(" ");

		/* print lane number */
		printf("|   %d  |", temp->lane_number);

		/* print lap times for laps completed */
		for (int m = 0; m < laps_complete; m++)
		{
			if (temp->lap_time[m] < 100.0)
				printf(" ");

			printf(" %0.3lf |", temp->lap_time[m]);
		}

		/* print running total time */
		if (temp->total_time < 100.0)
			printf(" ");

		printf(" %0.3lf |\n", temp->total_time);

		/* increment to the next runner*/
		temp = temp->next;
	}

	printf("\n");
}

/*
 * Prints a formatted representation of race results after the race is over
 */
void print_final(struct node** head, int32_t runners)
{
	double lane_order[runners];	/* array of runner's total times, for sorting and ordering the final output */
	double temp_val;			/* temporary value, used for swaping total race times in the sorting portion */
	struct node* temp = *head;

	/* scrape total times from linked list */
	for (int i = 0; i < runners; i++)
	{
		lane_order[i] = temp->total_time;
		temp = temp->next;
	}

	/* sort array */
	for (int j = 0; j < runners; j++)
	{
		for (int k = j; k < runners; k++)
		{
			if (lane_order[k] < lane_order[j])
			{
				temp_val = lane_order[k];
				lane_order[k] = lane_order[j];
				lane_order[j] = temp_val;
			}
		}
	}

	/* print by sorted total times */
	printf("\nFinal Result:\n");
	printf("Pos | Name                | School             | Lane |  Total  |\n");
	printf("====|=====================|====================|======|=========|\n");

	for (int m = 0; m < runners; m++)
	{
		temp = *head;

		/* find next runner */
		while (temp->total_time != lane_order[m])
			temp = temp->next;

		/* print position */
		printf(" %d  | ", m+1);

		/* print name */
		printf("%s", temp->name);

		/* fill empty spaces */
		for (int i = 0; i < (20 - strlen(temp->name)); i++)
			printf(" ");

		/* print school */
		printf("| %s", temp->school);

		/* fill empty spaces */
		for (int j = 0; j < (20 - strlen(temp->school)); j++)
			printf(" ");

		/* print lane number */
		printf("|   %d  |", temp->lane_number);

		/* print total time */
		if (temp->total_time < 100.0)
			printf(" ");

		printf("  %0.3lf  |\n", temp->total_time);
	}
}

/*
 * Clean up the doubly linked list accessed by head
 */
void destroy_dll(struct node** head)
{
	struct node* temp = *head;
	
	/* find the end of the doubly linked list */
	while(temp->next != NULL)
		temp = temp->next;

	/* free each node in reverse order */
	while(temp != NULL)
	{
		free(temp->next);
		temp->next = NULL;
		temp = temp->prev;
	}

	/* free the head node */
	free(*head);
	*head = NULL;
}

int main(void)
{
	char ctl_buff[32];				/* Buffer for receiving user input for control functions within main() */
	char name_buff[32];				/* Buffer for receiving user input for runner's name */
	char school_buff[32];			/* Buffer for receiving user input for runner's school name */

	int fd;							/* File descriptor for device file of hw5_kern module driver */
	int flag = 1;					/* Flag for staying in the pre-start loop */
	int retval = -1;				/* Return value for main() */

    int32_t runners = 0;			/* Count of runners in the race */
    int32_t uibuff[8];				/* Buffer for receiving kernel data. Holds one lap's worth of data (lap times) for up to 8 runners. */
    
    double qual_time_buff = 0.0;	/* Buffer for receving user input for runner's qualifying time */
    
    struct node* head = NULL;		/* Head of the doubly linked list holding all runners and runner's data */

  	
    printf("Welcome to the ECE 446 HW5 Race Timing System!\n");

    /* add runners to ll */
    printf("Enter the information for at least 2 and up to 8 runners.\n");
    for (int i = 0; i < 8; i++)
    {
    	printf("Enter the information the information for runner in lane %d.\n", i+1);
    	/* obtain and store runner's name in name_buff */
    	printf("Enter runner's name: ");
    	scanf("%[^\n]s", name_buff);
    	getchar();

    	/* obtain and store runner's school name in school_buff */
    	printf("Enter runner's school: ");
    	scanf("%[^\n]s", school_buff);
		getchar();    	

		/* obtain and store runner's qualifying time in qual_time_buff */
		printf("Enter runner's qualifying time: ");
    	scanf("%lf", &qual_time_buff);
    	getchar();

    	/* create new node for runner and append to the end of the doubly linked list accessed by head*/
    	append(&head, name_buff, school_buff, qual_time_buff);
    	/* increment the count of runners in the race */
    	runners++;

    	/* check for number of runners and ask for user input regarding adding further runners */
    	if ((runners >= 2) && (runners < 8))
    	{
    		printf("\nWould you like to add another runner (y/n)? ");
    		scanf("%s", ctl_buff);
    		getchar();
    		if (strcmp(ctl_buff, "n") == 0)
    			i = 8;
    	}
    	printf("\n");
    }

    /* wait for user to be ready to start race*/
    while(flag)
    {
    	printf("To start the race, enter START\n");
    	scanf("%s", ctl_buff);
    	getchar();
    	if (strcmp(ctl_buff, "START") == 0)
    	{
    		flag = 0;
    		print_start(&head);
    		sleep(1);
    		printf("3\n");
    		sleep(1);
    		printf("2\n");
    		sleep(1);
    		printf("1\n");
    		sleep(1);
    		printf("GO!\n\n");
    	}
    }

    /* insmod hw5_kern module */
    system("./ins_kern.sh > /dev/null");

    /* open hw5_kern device file */
    if ((fd = open("/dev/hw5_device", O_RDWR)) < 0) 
    {
        perror("Cannot open device file");

        if (errno == 2)
        	printf("Remember to use sudo to launch the hw5_user program!\n");

        goto open_dev_file_error;
    }

    /* write the number of runners to the hw5_kern module */
    /* note: this is not strictly necessary, hw5_kern will default to 8 runners */
    ioctl(fd, U2K, (int32_t*) &runners); 	

    /* for each lap specified by LAPS */
    for (int j = 0; j < LAPS; j++)
    {
    	/* read the lap data from the hw5_kern module */
    	ioctl(fd, K2U, (int32_t*) &uibuff);

    	/* update each runner's latest lap time */
    	for (int k = 0; k < runners; k++)
    	{
    		update_lap_time(&head, k+1, SEC(uibuff[k]));
    	}

    	/* print the current results */
    	print_current(&head);
    }
    
    /* when the race is over print the full results */
    print_final(&head, runners);
    retval = 0;

    /* close the hw5_kern module device file */
	close(fd);
open_dev_file_error:
    /* rmmod hw5_kern module */
	system("./rm_kern.sh > /dev/null");
    /* free linked list */
	destroy_dll(&head);

	return retval;
}