
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/slab.h>                 
#include <linux/uaccess.h>              
#include <linux/ioctl.h>

MODULE_LICENSE("Dual BSD/GPL");

#define U2K _IOW('z', 'a', int32_t*)	/* copy_from_user, user to kernel */
#define K2U _IOR('z', 'b', int32_t*)	/* copy_to_user, kernel to user */

/* globals */
int32_t runners = 0;
int32_t kibuff[8];

dev_t dev = 0;
static struct class *dev_class;
static struct cdev hw5_cdev;

/* Represents one lap of the race in a doubly linked list */
struct knode
{
    int lap_num;
    int lane_jiffs[8];

    struct knode* next; 
    struct knode* prev;
};

struct knode* head = NULL;
struct knode* last_lap = NULL;

/* 
 * Add a node to the end of the doubly linked list accessed by head.
 */
void append(void)
{
    /* allocate memory for new node */
    struct knode* new_node = (struct knode*) kmalloc(sizeof(struct knode), GFP_KERNEL);
    //last_lap = *head;

    new_node->next = NULL;

    /* if the list is currently empty, make the new node the head of the list */
    if (head == NULL)
    {
        new_node->lap_num = 1;
        new_node->prev = NULL;
        head = new_node;
    }
    else
    {
        /* append the new node to the last node and populate the new node's lap number */
        new_node->prev = last_lap;
        last_lap->next = new_node;
        new_node->lap_num = last_lap->lap_num + 1;
    }

    /* update the last lap node */
    last_lap = new_node;
}

/*
 * Clean up the doubly linked list accessed by head
 */
void kdestroy_dll(void)
{
    while(last_lap != NULL)
    {
        kfree(last_lap->next);
        last_lap->next = NULL;
        last_lap = last_lap->prev;
    }

    kfree(head);
    head = NULL;
}

/*
 * This fuction will be called when we call ioctl on the device file
 */
static long hw5_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int i = 0;
    switch(cmd) 
    {
        case U2K:
            copy_from_user(&runners, (int32_t*) arg, sizeof(runners));            
            break;
        case K2U:
            // append new node to ll
            append();
            for (i = 0; i < runners; i++)
                last_lap->lane_jiffs[i] = 15000 - i;
            copy_to_user((int32_t*) arg, &last_lap->lane_jiffs, sizeof(&last_lap->lane_jiffs)*runners);
            break;
    }
    return 0;
}

/*
 * File operation sturcture
 */
static struct file_operations fops =
{
    .owner = THIS_MODULE,
    .unlocked_ioctl = hw5_ioctl
};

/*
 * Module init function
 */
int hw5_init(void)
{
	int retval = -1;

    /* dynamically allocating major number */
    if ((alloc_chrdev_region(&dev, 0, 1, "hw5_Dev")) <0)
    {
        printk(KERN_INFO "(hw5) Cannot allocate major number\n");
        goto maj_num_error;
    }

    /* create cdev structure */
    cdev_init(&hw5_cdev,&fops);

    /* add cdev to the system */
    if ((cdev_add(&hw5_cdev, dev, 1)) < 0)
    {
        printk(KERN_INFO "(hw5) Cannot add the device to the system\n");
        goto cdev_error;
    }

    /* create class */
    if ((dev_class = class_create(THIS_MODULE, "hw5_class")) == NULL)
    {
        printk(KERN_INFO "(hw5) Cannot create the struct class\n");
        goto class_error;
    }

    /* create device */
    if ((device_create(dev_class, NULL, dev, NULL, "hw5_device")) == NULL)
    {
        printk(KERN_INFO "(hw5) Cannot create the Device 1\n");
        goto dev_error;
    }
    printk(KERN_INFO "(hw5) HW5 kernel module loaded and started.\n");
    
    retval = 0;
    goto success;
 
dev_error:
    class_destroy(dev_class);
class_error:
cdev_error:
    unregister_chrdev_region(dev, 1);
maj_num_error:
success:
    return retval;
}

/*
 * Module cleanup function
 */
void hw5_exit(void)
{
    kdestroy_dll();
	device_destroy(dev_class, dev);
    class_destroy(dev_class);
    cdev_del(&hw5_cdev);
    unregister_chrdev_region(dev, 1);
    printk(KERN_INFO "(hw5) HW5 kernel module unloaded.\n");
}

module_init(hw5_init);
module_exit(hw5_exit);
