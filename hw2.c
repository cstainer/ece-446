#include <stdio.h>
#include <stdlib.h>

/**
 *	Doubly linked list (dll) node with data fields to represent student runners
 */
struct node
{
	/* data fields */
	int bib;			/* bib number of the runner */
	int lane;			/* lane number of the runner */
	double qual_time;	/* runner's qualifying time in seconds */
	char* name;		/* name of the runner */
	char* school;	/* name of the runner's school */

	/* utility fields */
	struct node* next;	/* pointer to the next node in the dll */
	struct node* prev;	/* pointer to the previous node in the dll */
};

/**
 *  \brief	Method used to add a node to the beginning of the dll
 *  \struct node** head		pointer to the head of the dll
 *	\int 	new_bib		bib number of the runner represented by the new node
 * 	\int 	new_lane	lane number of the runner represented by the new node
 *	\double	new_qual_time	qualifying time of the runner represented by the
 							new node
 *	\char*	new_name	name of the runner represented by the new node
 *	\char*  new_school	name of the school attended by the runner represented
 						by the new node
 */
void insert_entry(struct node** head, 
				  int new_bib,
				  int new_lane,
				  double new_qual_time,
				  char* new_name,
				  char* new_school)
{
	/* Create and populate the new node */	
	struct node* new_node = (struct node*) malloc(sizeof(struct node));
	new_node->bib = new_bib;
	new_node->lane = new_lane;
	new_node->qual_time = new_qual_time;
	new_node->name = new_name;	//todo strcpy?
	new_node->school = new_school;	//todo strcpy?

	/* Attach the new node to the front of the list */
	new_node->next = *head;
	new_node->prev = NULL;

	if((*head) != NULL)
	{
		(*head)->prev = new_node;
	}

	/* Redefine the head to be the new node */
	(*head) = new_node;
}

/**
 *	\brief	Method used to remove the last node in the dll
 *	\struct node** head		pointer to the head of the dll
 */
void remove_entry(struct node** head)
{
	/* make a copy the head to preserve the head pointer */
	struct node* ptr = (*head);

	/* if the dll is empty */
	if((*head) == NULL)
	{
		/* do nothing */
	}
	/* if the dll has only one node */
	else if (ptr->next == NULL)
	{
		/* remove the data associated with the node */
		(*head) = NULL;
	}
	/* if the dll has more than one node */
	else
	{
		/* iterate through the dll to the final node */
		while(ptr->next != NULL)
		{
			ptr = ptr->next;
		}

		/* remove the last node's link to the dll and free memory */
		ptr->prev->next = NULL;
		free(ptr);
	}
}

/**
 *	\brief	Method used to print the entire dll
 */
void print_list(struct node** head)
{
	/* make a copy the head to preserve the head pointer */
	struct node* ptr = (*head);

	printf("Printing the list...\n");

	/* iterate through the dll, printing all the data fields of each valid node
	   along the way */
	while(ptr != NULL)
	{
		printf("\nbib = %d\n", ptr->bib);
		printf("lane = %d\n", ptr->lane);
		printf("qual_time = %.3f\n", ptr->qual_time);
		printf("name = %s\n", ptr->name);
		printf("school = %s\n\n", ptr->school);
		ptr = ptr->next;
	}

	printf("List printed!\n\n");
}

int main (void)
{
	printf("DLL assignment begins\n\n");

	/* create the dll */
	struct node* head = NULL;

	/* print the dll, showing the empty dll */
	printf("Empty list:\n");
	print_list(&head);

	/* add nodes to the dll */
	insert_entry(&head, 44, 1, 101.252, "Lewis Hamilton", "Mercedes");	
	insert_entry(&head, 77, 2, 101.763, "Valtteri Bottas", "Mercedes");
	insert_entry(&head, 33, 3, 101.778, "Max Verstappen", 
				 "Red Bull Racing-Honda");
	insert_entry(&head, 3, 4, 102.061, "Daniel Ricciardo", "Renault");
	insert_entry(&head, 23, 5, 102.264, "Alexander Albon",
				 "Red Bull Racing-Honda");
	insert_entry(&head, 31, 6, 102.396, "Esteban Ocon", "Renault");
	insert_entry(&head, 55, 7, 102.438, "Carlos Sainz Jr.", "McLaren-Renault");
	insert_entry(&head, 11, 8, 102.532, "Sergio Perez", 
				 "Racing Point-BWT Mercedes");

	/* print the dll showing 8 runners */
	printf("DLL with 8 runners:\n");
	print_list(&head);

	/* remove 4 runners from the dll */
	for(int i = 0; i < 4; i++)
	{
		remove_entry(&head);
	}

	/* print the dll showing 4 runners */
	printf("DLL with 4 runners:\n");
	print_list(&head);

	printf("DLL assignment completed\n");
	return 0;
}
