#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("Dual BSD/GPL");

/*
 *  Setting a default num value, also allowing for non-default values
 */
static int num = 7;
module_param(num, int, S_IRUGO);

/*
 *  Startup function.  Prints out either the default or provided number.
 */
static int hw3_init(void)
{
    printk(KERN_ALERT "(HW3) Hello, my favorite number is %d.\n", num);
    return 0;
}

/*
 *  Exit function.
 */
static void hw3_exit(void)
{
    printk(KERN_ALERT "(HW3) Goodbye.\n");
}

module_init(hw3_init);
module_exit(hw3_exit);
