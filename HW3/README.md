# Assignment 
  - ECE 446 Device Driver Development
  - Homework 3
  - Charles Stainer
  - Septemnber 26

# Build
Download `hw3.c` and its associated `Makefile`, or clone this Gitlab repo:

```sh
$ git clone git@gitlab.com:cstainer/ece-446.git
```

Navigate to the downloaded directory, then run `make` to compile the module. In this case, I cloned it to my desktop.

```sh
$ cd Desktop/ece-446/HW3/
$ make
```

# Load
To load the compiled module with the default value, navigate to the location of the compiled object file. Assuming you just finished building it, you should be there already.  Run:

```sh
$ sudo insmod ./hw3.ko
```

It will ask you for your password; after entering your password the module will be loaded.

To load the module with a non-default integer value, run:

```sh
$ sudo insmod ./hw3.ko num=x
```

where `x` is the non-default integer value.

# Unload
To unload the module, whether with a default value or not, run:

```sh
$ sudo rmmod hw3
```

After unloading the module, if you wish to clean up the folder, run:

```sh
$ make clean
```

# Locating the module's messages
For ease of sifting through the zillion lines of `/var/log/syslog`, I included `(HW3)` at the beginning of each printout.  In `/var/log/` run:

```sh
$ cat syslog | grep HW3
```

# Source code
## HW3.c
```c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("Dual BSD/GPL");

/*
 *  Setting a default num value, also allowing for non-default values
 */
static int num = 7;
module_param(num, int, S_IRUGO);

/*
 *  Startup function.  Prints out either the default or provided number.
 */
static int hw3_init(void)
{
    printk(KERN_ALERT "(HW3) Hello, my favorite number is %d.\n", num);
    return 0;
}

/*
 *  Exit function.
 */
static void hw3_exit(void)
{
    printk(KERN_ALERT "(HW3) Goodbye.\n");
}

module_init(hw3_init);
module_exit(hw3_exit);
```
HW3.c was heavily influenced by the code found at the following Github repo: https://github.com/martinezjavier/ldd3.  `ldd3/misc-modules/hello.c` and `ldd3/misc-modules/hellop.c` were especially helpful.  


## Makefile
```
# If KERNELRELEASE is defined, we've been incoked from the
# kernel build system and can use its language.
ifneq ($(KERNELRELEASE),)
    obj-m := hw3.o

# Otherwise we were called directly fom the command
# line; invoke the kernel build system.
else

    KERNELDIR ?= /lib/modules/$(shell uname -r)/build
    PWD := $(shell pwd)

default:
    $(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
    rm -r *.ko *.mod *.mod.c *.o *.symvers *.order

endif
```
The Makefile was copied almost verbatim from LDD3 Chapter 2, page 24.  I added the `clean` functionality and changed the name of `obj-m`.

# $PATH
`/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin`
