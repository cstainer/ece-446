# Assignment 
  - ECE 446 Device Driver Development
  - Homework 4
  - Charles Stainer
  - October 31, 2020

# Build
Download `hw4.c` and its associated `Makefile`, or clone this Gitlab repo:

```sh
$ git clone git@gitlab.com:cstainer/ece-446.git
```

Navigate to the downloaded directory, then run `make` to compile the module. In this case, I cloned it to my desktop.

```sh
$ cd Desktop/ece-446/HW4/
$ make
```

# Load
To load the compiled module, navigate to the location of the compiled object file. Assuming you just finished building it, you should be there already.  Run:

```sh
$ sudo insmod ./hw4.ko
```

It will ask you for your password; after entering your password the module will be loaded.

# Unload
To unload the module, run:

```sh
$ sudo rmmod hw3
```

After unloading the module, if you wish to clean up the folder, run:

```sh
$ make clean
```

# Locating the module's messages
For ease of sifting through the zillion lines of `/var/log/syslog`, I included `(HW4)` at the beginning of each printout.  In `/var/log/` run:

```sh
$ cat syslog | grep HW4
```

# Source code
## HW4.c
```c
#include <linux/semaphore.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/types.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define EXIT_WARNING 2

/**
 *  Singly linked list (sll) node with data fields to represent runners in a 
 *  4 lap race, such as a standard 1600 meter race.
 */
struct node
{
  /* Data fields */
  u8 number;  /* Runner's ID number */
  u8 lane;    /* Lane number of the runner */
  u64 race;   /* Total race time in jiffies (sum of the lap times) */
  u64 lap[4]; /* Lap time for each lap in jiffies */

  /* Utility fields */
  struct node* next;
};


MODULE_LICENSE("Dual BSD/GPL");

/* Global variables */
struct node* head;      /* Head of the linked list */
u64 start;              /* Start time of the race in jiffies */
struct semaphore *sem;  /* Semaphore for accessing the linked list */

/**
 *  \brief  Insert a new node at the top of the linked list.
 *  \u8   new_lane  The lane number to assign to the new node
 *  \u8   new_number  The ID number of the runner
 *  \return u8  EXIT_FAILURE on failure to allocate memory or obtain lock, 
 *              else EXIT_SUCCESS
 */
static u8 insert_new_node(u8 new_lane, u8 new_number)
{
  u8 retval = EXIT_FAILURE;
  u8 i = 0;

  /* Allocate memory for the new node */
  struct node* new_node = (struct node*) kmalloc(sizeof(struct node), GFP_KERNEL);

  /* Error check the memory allocation */
  if (new_node == NULL)
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to allocate memory.", __LINE__);
    kfree(new_node);
    goto mem_fail;
  }

  /* Obtain the lock */
  if (EXIT_SUCCESS != down_interruptible(sem))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to obtain lock.", __LINE__);
    kfree(new_node);
    goto lock_fail;
  }

  /* Initialize the new node's data fields */
  new_node->number = new_number;
  new_node->lane = new_lane;
  new_node->race = 0;

  for (i = 0; i < 4; i++)
    new_node->lap[i] = 0; 

  /* Edge case: the list is empty*/
  if (head == NULL)
  {
    head = new_node;
    head->next = NULL;
    goto success;
  }

  /* Insert the new node at the top of the list */
  new_node->next = head->next;
  head->next = new_node;

success:
  up(sem);  /* Release the lock */
  retval = EXIT_SUCCESS;
lock_fail:
mem_fail:
  return retval;  
}


/**
 *  \brief  Remove the node with number = rem_num
 *  \u8   rem_num   The ID number of the node to be removed
 *  \return u8  EXIT_FAILURE on failure to obtain lock, 
 *              EXIT_WARNING on attempt to remove non-existant node
 *              else EXIT_SUCCESS
 */
static u8 update_node(u8 number, u64 lap_time)
{
  u8 retval = EXIT_FAILURE;
  struct node *ptr = head;

  /* Obtain the lock */
  if (EXIT_SUCCESS != down_interruptible(sem))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to obtain lock.", __LINE__);
    goto lock_fail;
  }

  /* Iterate through the list */
  while(ptr != NULL)
  {
    /* If the number to be updated is discovered */
    if (ptr->number == number)
    {
      /* If there is no time entered for lap 1, then we have just completed lap 1. */
      if (ptr->lap[0] == 0)
      {
        ptr->lap[0] = lap_time - start;
        ptr->race += ptr->lap[0];
        goto success;
      }
      /* If there is no time entered for lap 2, then we have just completed lap 2. */
      else if (ptr->lap[1] == 0)
      {
        ptr->lap[1] = lap_time - start - (ptr->race);
        ptr->race += (ptr->lap[1]);
        goto success;
      }
      /* If there is no time entered for lap 3, then we have just completed lap 3. */
      else if (ptr->lap[2] == 0)
      {
        ptr->lap[2] = lap_time - start - (ptr->race);
        ptr->race += (ptr->lap[2]);
        goto success;
      }
      /* If there is no time entered for lap 4, then we have just completed lap 4. */
      else if (ptr->lap[3] == 0)
      {
        ptr->lap[3] = lap_time - start - (ptr->race);
        ptr->race += (ptr->lap[3]);
        goto success;
      }
      /* If there is no time entered for lap 4, then the race is complete (return warning) */
      else
      {
        break;  /* Do not continue iterating through the list. */
      }
    }

    ptr = ptr->next;  /* Iterate through the list */
  }

  /* 
   * If we reached this point, we have not found the number to be updated, 
   * or it is not able to be updated. 
   */
  retval = EXIT_WARNING;
  printk(KERN_ALERT "(HW4) Line %d: Warning: attempted to update node that does "
    "not exist or is not able to be updated.", __LINE__);
  goto update_fail; 

success:
  retval = EXIT_SUCCESS;
update_fail:
  up(sem);  /* Release the lock */
lock_fail:
  return retval;  
}

/**
 *  \brief  Remove the node with number = rem_num
 *  \u8   rem_num   The ID number of the node to be removed
 *  \return u8  EXIT_FAILURE on failure to obtain lock, 
 *              EXIT_WARNING on attempt to remove non-existant node
 *              else EXIT_SUCCESS
 */
static u8 remove_node(u8 rem_num)
{
  u8 retval = EXIT_FAILURE;
  struct node *ptr = head;
  struct node *temp;

  /* Obtain the lock */
  if (EXIT_SUCCESS != down_interruptible(sem))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to obtain lock.", __LINE__);
    goto lock_fail;
  }

  /* Edge case: removing the head */
  if (ptr->number == rem_num)
  {
    head = ptr->next;
    kfree(ptr);
    goto success;
  }

  /* Iterate through the list */
  while(ptr != NULL)
  {
    /* If the number to be removed is discovered*/
    if (ptr->next->number == rem_num)
    {
      temp = ptr->next;       /* Save the pointer to the node to be removed*/
      ptr->next = ptr->next->next;  /* Remove the node from the list */
      kfree(temp);          /* Free the memory occupied by the node */
      goto success;
    }

    ptr = ptr->next;  /* Iterate through the list */
  }

  /* If we reached this point, we have not found the number to be removed. */
  retval = EXIT_WARNING;
  printk(KERN_ALERT "(HW4) Line %d: Warning: attempted to remove node " 
    "that does not exist.", __LINE__);
  goto rem_fail;  

success:
  retval = EXIT_SUCCESS;
rem_fail:
  up(sem);  /* Release the lock */
lock_fail:
  return retval;  
}

/**
 *  \brief  Remove and free all nodes, leave head allocated and equal to NULL
 *  \return u8  EXIT_FAILURE on failure to obtain lock, 
 *              else EXIT_SUCCESS
 */
static u8 remove_all(void)
{
  u8 retval;
  struct node *ptr = head;

  /* Obtain the lock */
  if (EXIT_SUCCESS != down_interruptible(sem))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to obtain lock.", __LINE__);
    goto lock_fail;
  }

  /* Iterate through the list, freeing each node in turn, leave head allocated (head = NULL) */
  while(ptr != NULL)
  {
    head = head->next;
    kfree(ptr);
    ptr = head;
  }

  retval = EXIT_SUCCESS;
  up(sem);  /* Release the lock */
lock_fail:
  return retval;  
}

/**
 *  /brief  Print the linked list to var/log/syslog
 */
static void print_list(void)
{
  struct node *ptr = head;

  /* Obtain the lock */
  if (EXIT_SUCCESS != down_interruptible(sem))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to obtain lock.", __LINE__);
    goto lock_fail;
  }

  while(ptr != NULL)
  {
    printk(KERN_ALERT "(HW4) Lane: %d, runner: %d, "
      "time: %lld.%lld, splits: %lld.%lld %lld.%lld %lld.%lld %lld.%lld\n", 
      ptr->lane, ptr->number, 
      (ptr->race)/HZ,   ((((ptr->race)*1000)/HZ)%1000),     /* Total race time */
      (ptr->lap[0])/HZ, ((((ptr->lap[0])*1000)/HZ)%1000),   /* Lap 1 split */
      (ptr->lap[1])/HZ, ((((ptr->lap[1])*1000)/HZ)%1000),   /* Lap 2 split */
      (ptr->lap[2])/HZ, ((((ptr->lap[2])*1000)/HZ)%1000),   /* Lap 3 split */
      (ptr->lap[3])/HZ, ((((ptr->lap[3])*1000)/HZ)%1000));  /* Lap 4 split */

    ptr = ptr->next;
  }

  up(sem);  /* Release the lock */
lock_fail:
  return;
}

/*
 *  HW4 Demo.  Demonstrates using linked lists, kernal data types, and locking
 *  mechanisms to store and display track runners' information.  Information 
 *  stored by the linked list includes lap times, total race time, and lane 
 *  number.  Lap times and total race time are updated and displayed using a 
 *  locking mechanism.
 */
static u8 hw4_demo(void)
{
  u8 retval = EXIT_FAILURE, i = 0;
  u64 temp;

  /* Add runner 44 to race */
  if (EXIT_SUCCESS != insert_new_node(1, 44))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to add runner.", __LINE__);
    goto fail;
  }

  /* Add runner 3 to race */
  if (EXIT_SUCCESS != insert_new_node(2, 3))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to add runner.", __LINE__);
    goto fail;
  }

  /* Add runner 7 to race */
  if (EXIT_SUCCESS != insert_new_node(3, 7))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to add runner.", __LINE__);
    goto fail;
  }

  /* Remove runner 7 from race */
  if (EXIT_SUCCESS != remove_node(7))
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to remove runner.", __LINE__);
    goto fail;
  }

  /* Start the race */
  start = jiffies;
  temp = start;

  /* Race 4 laps */
  for (i = 0; i < 4; i++)
  {
    /* Wait for the first runner */
    temp += 375;
    while(jiffies < temp)
    {
      // NOP, busy wait
    }
    
    update_node(44, jiffies); /* Update runner 44 with the current time*/
    
    /* Wait for the second runner */
    temp += 125;
    while(jiffies < temp)
    {
      // NOP, busy wait
    }
    
    update_node(3, jiffies);  /* Update runner 3 with current time */
  }

  /* Finish the race (display results) */
  print_list();

  retval = EXIT_SUCCESS;
fail:
  return retval;
}

/*
 *  Startup function.
 */
static int hw4_init(void)
{
  u8 retval = EXIT_FAILURE;

  /* Allocate memory for the semaphore */
  sem = (struct semaphore*) kmalloc(sizeof(struct semaphore), GFP_KERNEL);

  /* Error check the memory allocation */
  if (sem == NULL)
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to allocate memory.", __LINE__);
    kfree(sem);   /* Ensure there are no memory leaks */
    goto fail;
  }

  sema_init(sem, 0);  /* Set up the semaphore as a mutex lock (locked) */

  head = NULL;    /* Initialize the linked list */
  
  up(sem);      /* Release the lock */

  /* Run demo for assignment */
  if (EXIT_SUCCESS != hw4_demo())
  {
    printk(KERN_ALERT "(HW4) Line %d: Failed to run demo.", __LINE__);
    goto fail;
  }

  retval = EXIT_SUCCESS;
fail:
    return (int) retval;
}

/*
 *  Exit function.
 */
static void hw4_exit(void)
{
    remove_all(); /* Delete linked list */
    kfree(head);  /* Free head */

    kfree(sem);   /* Destroy the semaphore */
}

module_init(hw4_init);
module_exit(hw4_exit);
```  

## Makefile
```
# If KERNELRELEASE is defined, we've been incoked from the
# kernel build system and can use its language.
ifneq ($(KERNELRELEASE),)
    obj-m := hw4.o

# Otherwise we were called directly fom the command
# line; invoke the kernel build system.
else

    KERNELDIR ?= /lib/modules/$(shell uname -r)/build
    PWD := $(shell pwd)

default:
    $(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
    rm -r *.ko *.mod *.mod.c *.o *.symvers *.order

endif
```
The Makefile was copied almost verbatim from LDD3 Chapter 2, page 24.  I added the `clean` functionality and changed the name of `obj-m`.

# $PATH
`/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin`
