#include <stdio.h>

int main()
{
    int myInt, *myPointerToInt;
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    myPointerToInt = &myInt; /* initialise pointer */
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    *myPointerToInt = 0; /* set myInt to zero */
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    printf("myInt is %d\n", myInt);
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    printf("*myPointerToInt is %d\n", *myPointerToInt);
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    *myPointerToInt += 1; /* increment what myPointerToInt points to */
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    printf("myInt is %d\n", myInt);
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    (*myPointerToInt)++; /* increment what myPointerToInt points to */
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);
    printf("myInt is %d\n", myInt);
    printf("line %d, myPointerToInt = %d, &myPointerToInt = %p\n", __LINE__, *myPointerToInt, myPointerToInt);

    return 0;
}